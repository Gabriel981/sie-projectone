package com.operational;

public class OperatiuniCalculComanda implements iOperatiuniCalculComanda {

	@Override
	public Double calculValoareLinieComanda(Integer cantitate, Double valoareUnitara) {
		if(cantitate > 0 && valoareUnitara > 0) {
			return valoareUnitara * cantitate;
		}
		
		return 0.00;
	}

	@Override
	public Double reverseValoareComanda(Integer cantitate, Double valoareUnitara) {
		if(cantitate > 0 && valoareUnitara > 0) {
			return valoareUnitara * cantitate;
		}
		
		return 0.00;
	}

}
