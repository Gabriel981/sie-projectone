package com.operational;

import com.models.Comanda;
import com.models.LinieComanda;
import com.viewModels.CreareElementActivViewModel;

public class OperatiuniComanda implements iOperatiuniComanda {

	private iOperatiuniCalculComanda operatiuniCalculComanda;
	
	public OperatiuniComanda() {
		super();
		this.operatiuniCalculComanda = new OperatiuniCalculComanda();
	}

	@Override
	public void actualizareValoareComanda(Comanda comanda, Double valoareNoua) {
		if(comanda != null && !valoareNoua.equals(null) && valoareNoua > 0.00) {
			comanda.updateValoareComanda(valoareNoua);
		}
	}

	@Override
	public Object addLinieComanda(Comanda comanda, LinieComanda linieComanda, CreareElementActivViewModel viewModel) {
		if(comanda != null && linieComanda != null) {
			Object elementActiv = linieComanda.incadrareElementActiv(viewModel);
			comanda.getLiniiComanda().add(linieComanda);
			Double calculLinie = this.operatiuniCalculComanda.calculValoareLinieComanda(linieComanda.getiCantitateComanda(), linieComanda.getdValoareUnitara());
			if(calculLinie > 0.00) {
				this.actualizareValoareComanda(comanda, calculLinie);
			}
			
			return elementActiv;
		}
		
		return null;
	}

	@Override
	public void deleteLinieComanda(Comanda comanda, LinieComanda linieComanda) {
		if(comanda != null && linieComanda != null) {
			if(comanda.getLiniiComanda().size() > 0 && comanda.getLiniiComanda().contains(linieComanda)) {
				comanda.getLiniiComanda().remove(linieComanda);
				Double reverseValoare = this.operatiuniCalculComanda.reverseValoareComanda(linieComanda.getiCantitateComanda(), linieComanda.getdValoareUnitara());
				if(reverseValoare > 0.00) {
					Double valoareNouaReverse = comanda.getdValoareComanda() - reverseValoare;
					if(valoareNouaReverse > 0) {
						this.actualizareValoareComanda(comanda, valoareNouaReverse);
					}
				}
			}
		}
	}
	
}
