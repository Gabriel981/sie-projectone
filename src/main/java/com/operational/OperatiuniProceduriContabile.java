package com.operational;

import java.util.List;

import com.models.Amortizare;
import com.models.AmortizareAccelerata;
import com.models.AmortizareDegresiva;
import com.models.AmortizareLiniara;
import com.models.ElementActiv;
import com.models.MijlocFix;
import com.models.ProceduraContabilaAmortizare;
import com.models.TipAmortizare;
import com.viewModels.CreareElementActivViewModel;
import com.viewModels.TipAmortizareViewModel;

public class OperatiuniProceduriContabile implements IOperatiuniProceduriContabile {

	@Override
	public TipAmortizareViewModel verificaMetodaAmortizareProcedura(List<ProceduraContabilaAmortizare> proceduriContabileAmortizare,
			ElementActiv elementActiv, CreareElementActivViewModel creareElementActiv) {
		
		if(elementActiv != null && elementActiv instanceof MijlocFix 
				&& proceduriContabileAmortizare != null && proceduriContabileAmortizare.size() > 0) {
			
			Amortizare amortizareRecomandata = null;
			TipAmortizareViewModel returnedModel = new TipAmortizareViewModel(null, null);
			for(ProceduraContabilaAmortizare proceduraContabila : proceduriContabileAmortizare) {
				if(proceduraContabila.getElementActivProcedura().getsDenumireElementActiv().toLowerCase().equals(elementActiv.getsDenumireElementActiv().toLowerCase()) && 
						proceduraContabila.getElementActivProcedura().getsAliasElementActiv().equals(elementActiv.getsAliasElementActiv()) && proceduraContabila.getTipAmortizareRecomandat() != null) {
					switch (proceduraContabila.getTipAmortizareRecomandat()) {
						case LINIARA:
							amortizareRecomandata = new AmortizareLiniara(TipAmortizare.LINIARA, creareElementActiv.getdValoareIntrare(), creareElementActiv.getdValoareRecuperare(), creareElementActiv.getiDurataUtilizare());
							returnedModel.setTipAmortizareAlocat(TipAmortizare.LINIARA);
							returnedModel.setAmortizareAlocata(amortizareRecomandata);
							break;
						case DEGRESIVA:
							amortizareRecomandata = new AmortizareDegresiva(TipAmortizare.DEGRESIVA, creareElementActiv.getdValoareIntrare(), creareElementActiv.getiDurataUtilizare());
							returnedModel.setTipAmortizareAlocat(TipAmortizare.DEGRESIVA);
							returnedModel.setAmortizareAlocata(amortizareRecomandata);
							break;
						case ACCELERATA:
							amortizareRecomandata = new AmortizareAccelerata(TipAmortizare.ACCELERATA, creareElementActiv.getdValoareIntrare(), null, creareElementActiv.getiDurataUtilizare());
							returnedModel.setTipAmortizareAlocat(TipAmortizare.ACCELERATA);
							returnedModel.setAmortizareAlocata(amortizareRecomandata);
							break;
						default:
							break;
					}
				}
			}
			
			return returnedModel;
		}
		
		return null;
		
	}

	

}
