package com.operational;

import com.models.AmortizareAccelerata;
import com.models.AmortizareDegresiva;
import com.models.AmortizareLiniara;
import com.models.IstoricAmortizare;

public class OperatiuniCalculAmortizare implements IOperatiuniCalculAmortizare {

	@Override
	public void calculValoareAmortizareLiniara(AmortizareLiniara amortizareLiniara, Double costIstoric, Double valoareRecuperare) {
		if(!amortizareLiniara.equals(null)) {
			if(!costIstoric.equals(null) && !valoareRecuperare.equals(null) && !valoareRecuperare.equals(null)) {
				Double valoareAmortizare = costIstoric - valoareRecuperare;
				if(valoareAmortizare > 0.00) {
					amortizareLiniara.setdValoareAmortizare(valoareAmortizare);
				}
			}
		}
	}
	
	@Override
	public void calculAmortizareLiniara(AmortizareLiniara amortizareLiniara, Double valoareAmortizare, Integer durataUtilizare) {
		// TODO Auto-generated method stub
		if(amortizareLiniara != null && !valoareAmortizare.equals(null) && !durataUtilizare.equals(null)) {
			Double amortizareAnuala = valoareAmortizare / durataUtilizare;
			if(amortizareAnuala > 0.00) {
				for(int i = 0; i < durataUtilizare; i++) {
					amortizareLiniara.getIstoricAmortizari().add(new IstoricAmortizare(i + 1, amortizareAnuala, i + 1, amortizareLiniara));
				}
				
				Double valoareCalculataAmortizare = 0.00;
				
				for(IstoricAmortizare istoricAmortizare : amortizareLiniara.getIstoricAmortizari()) {
					if(istoricAmortizare != null) {
						valoareCalculataAmortizare += istoricAmortizare.getdValoareAmortizareRezultata();
					}
				}
								
				if(valoareCalculataAmortizare > 0.00) {
					amortizareLiniara.setdValoareCalculataAmortizare(valoareCalculataAmortizare);
				}
				
				Double rataAmortizare = (amortizareAnuala / valoareAmortizare) * 100;
				if(rataAmortizare > 0) {
					amortizareLiniara.setdRataAmortizare(rataAmortizare);
				}
			}
		}
	}

	@Override
	public void calculAmortizareDegresiva(AmortizareDegresiva amortizareDegresiva, Double valoareAmortizare,
			Integer durataUtilizare) {
		
		if(amortizareDegresiva != null && !valoareAmortizare.equals(null) && !durataUtilizare.equals(null)) {
			Double coeficientGenerat = this.getCoeficientAmortizareGenerat(durataUtilizare);
			if(coeficientGenerat > 0.00) {
				Double rataAnualaAmortizareLiniar = 100.0 / durataUtilizare;
				Double rataAmortizareDegresiva = rataAnualaAmortizareLiniar * coeficientGenerat;
				for(int i = 0; i < durataUtilizare; i++) {
					Integer durataUtilizareRamasa = durataUtilizare - i;
					Double amortizareLiniaraAnuala = valoareAmortizare / durataUtilizareRamasa;
					Double amortizareCalculataCurent = (valoareAmortizare * rataAmortizareDegresiva) / 100;
					
					if(amortizareCalculataCurent < amortizareLiniaraAnuala) {
						amortizareDegresiva.getIstoricAmortizare().add(new IstoricAmortizare(i + 1, amortizareLiniaraAnuala, i + 1, amortizareDegresiva));
						valoareAmortizare -= amortizareLiniaraAnuala;
					} else {
						amortizareDegresiva.getIstoricAmortizare().add(new IstoricAmortizare(i + 1, amortizareCalculataCurent, i + 1, amortizareDegresiva));
						valoareAmortizare -= amortizareCalculataCurent;
					}
				}
				
				Double valoareCalculataAmortizare = 0.00;
				
				for(IstoricAmortizare istoricAmortizare : amortizareDegresiva.getIstoricAmortizare()) {
					if(istoricAmortizare != null) {
						valoareCalculataAmortizare += istoricAmortizare.getdValoareAmortizareRezultata();
					}
				}
				
				if(valoareCalculataAmortizare > 0.00) {
					amortizareDegresiva.setdValoareCalculataAmortizare(valoareCalculataAmortizare);
				}
			}
		}
		
	}
	
	@Override
	public void calculAmortizareAccelerata(AmortizareAccelerata amortizareAccelerata, Double valoareAmortizare,
			Double rataAmortizare, Integer durataUtilizare) {
		// TODO Auto-generated method stub
		if(amortizareAccelerata != null && !valoareAmortizare.equals(null) && !rataAmortizare.equals(null)) {
			if(rataAmortizare <= 0.5 || rataAmortizare <= 50) {
				Double amortizareCalculataPrimulAnDegresiv = valoareAmortizare * rataAmortizare;
				if(amortizareCalculataPrimulAnDegresiv > 0.00) {
					amortizareAccelerata.getIstoricAmortizare().add(new IstoricAmortizare(1, amortizareCalculataPrimulAnDegresiv, 1, amortizareAccelerata));
					valoareAmortizare -= amortizareCalculataPrimulAnDegresiv;

					Double amortizareRamasaCalculataLiniar = valoareAmortizare / (durataUtilizare - 1);
					
					for(int i = 1; i <= durataUtilizare - 1; i++) {
						amortizareAccelerata.getIstoricAmortizare().add(new IstoricAmortizare(i + 1, amortizareRamasaCalculataLiniar, i + 1, amortizareAccelerata));
						valoareAmortizare -= amortizareRamasaCalculataLiniar;
					}
				}

				Double valoareAmortizareCalculata = 0.0;
				for(IstoricAmortizare istoricAmortizare : amortizareAccelerata.getIstoricAmortizare()) {
					if(istoricAmortizare != null) {
						valoareAmortizareCalculata += istoricAmortizare.getdValoareAmortizareRezultata();
					}
				}
				
				if(valoareAmortizareCalculata > 0.00) {
					amortizareAccelerata.setdValoareCalculataAmortizare(valoareAmortizareCalculata);
				}
			}
		}
	}


	private Double getCoeficientAmortizareGenerat(Integer durataUtilizare) {
		Double rataAmortizareGenerata = 0.00;
		if(durataUtilizare != null && durataUtilizare >= 2) {
			if(durataUtilizare >= 2 && durataUtilizare <= 5) {
				rataAmortizareGenerata = 1.5;
			}
			
			if(durataUtilizare >= 6 && durataUtilizare <= 10) {
				rataAmortizareGenerata = 2.0;
			}
			
			if(durataUtilizare > 10) {
				rataAmortizareGenerata = 2.5;
			}
		}
		
		return rataAmortizareGenerata;
	}

	@Override
	public void test() {
		// TODO Auto-generated method stub
		System.out.println("test");
	}
}
