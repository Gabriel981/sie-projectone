package com.operational;

import java.util.List;

import com.models.ElementActiv;
import com.models.ProceduraContabilaAmortizare;
import com.viewModels.CreareElementActivViewModel;
import com.viewModels.TipAmortizareViewModel;

public interface IOperatiuniProceduriContabile {
	TipAmortizareViewModel verificaMetodaAmortizareProcedura(List<ProceduraContabilaAmortizare> proceduriContabileAmortizare, ElementActiv elementActiv, CreareElementActivViewModel creareElementActivSix);
}
