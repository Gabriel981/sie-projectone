package com.operational;

import com.models.Comanda;
import com.models.LinieComanda;
import com.viewModels.CreareElementActivViewModel;

public interface iOperatiuniComanda {
	void actualizareValoareComanda(Comanda comanda, Double valoareNoua);
	Object addLinieComanda(Comanda comanda, LinieComanda linieComanda, CreareElementActivViewModel viewModel);
	void deleteLinieComanda(Comanda comanda, LinieComanda linieComanda);
}
