package com.operational;

import com.models.AmortizareAccelerata;
import com.models.AmortizareDegresiva;
import com.models.AmortizareLiniara;

public interface IOperatiuniCalculAmortizare {
	void calculValoareAmortizareLiniara(AmortizareLiniara amortizareLiniara, Double costIstoric, Double valoareRecuperare);
	void calculAmortizareLiniara(AmortizareLiniara amortizareLiniara, Double valoareAmortizare, Integer durataUtilizare);
	void calculAmortizareDegresiva(AmortizareDegresiva amortizareDegresiva, Double valoareAmortizare, Integer durataUtilizare);
	void calculAmortizareAccelerata(AmortizareAccelerata amortizareAccelerata, Double valoareAmortizare, Double rataAmortizare, Integer durataUtilizare);
	void test();
}
