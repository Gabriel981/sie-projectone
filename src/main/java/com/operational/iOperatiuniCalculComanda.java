package com.operational;

public interface iOperatiuniCalculComanda {
	Double calculValoareLinieComanda(Integer cantitate, Double valoareUnitara);
	Double reverseValoareComanda(Integer cantitate, Double valoareUnitara);
}
