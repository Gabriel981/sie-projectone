package com.models;

import java.util.Date;
import java.util.UUID;

public class ProceduraContabilaAmortizare {
	private String sProceduraContabilaId;
	private String sDenumireProceduraContabila;
	private Date dtDataIntocmireProceduraContabila;
	private ElementActivProcedura elementActivProcedura;
	private TipAmortizare tipAmortizareRecomandat;
	
	public ProceduraContabilaAmortizare(String sDenumireProceduraContabila,
			Date dtDataIntocmireProceduraContabila, ElementActivProcedura elementActivProcedura, TipAmortizare tipAmortizareRecomandat) {
		super();
		this.sDenumireProceduraContabila = sDenumireProceduraContabila;
		this.dtDataIntocmireProceduraContabila = dtDataIntocmireProceduraContabila;
		this.elementActivProcedura = elementActivProcedura;
		this.tipAmortizareRecomandat = tipAmortizareRecomandat;
		this.setsProceduraContabilaId(UUID.randomUUID().toString());
	}

	public String getsProceduraContabilaId() {
		return sProceduraContabilaId;
	}
	
	public void setsProceduraContabilaId(String iProceduraContabilaId) {
		this.sProceduraContabilaId = iProceduraContabilaId;
	}

	public String getsDenumireProceduraContabila() {
		return sDenumireProceduraContabila;
	}

	public void setsDenumireProceduraContabila(String sDenumireProceduraContabila) {
		this.sDenumireProceduraContabila = sDenumireProceduraContabila;
	}

	public Date getDtDataIntocmireProceduraContabila() {
		return dtDataIntocmireProceduraContabila;
	}

	public void setDtDataIntocmireProceduraContabila(Date dtDataIntocmireProceduraContabila) {
		this.dtDataIntocmireProceduraContabila = dtDataIntocmireProceduraContabila;
	}

	public ElementActivProcedura getElementActivProcedura() {
		return elementActivProcedura;
	}

	public void setElementActivProcedura(ElementActivProcedura elementActivProcedura) {
		this.elementActivProcedura = elementActivProcedura;
	}

	public TipAmortizare getTipAmortizareRecomandat() {
		return tipAmortizareRecomandat;
	}

	public void setTipAmortizareRecomandat(TipAmortizare tipAmortizareRecomandat) {
		this.tipAmortizareRecomandat = tipAmortizareRecomandat;
	}

	@Override
	public String toString() {
		return "ProceduraContabilaAmortizare [sProceduraContabilaId=" + sProceduraContabilaId
				+ ", sDenumireProceduraContabila=" + sDenumireProceduraContabila
				+ ", dtDataIntocmireProceduraContabila=" + dtDataIntocmireProceduraContabila
				+ ", elementActivProcedura=" + elementActivProcedura + ", tipAmortizareRecomandat="
				+ tipAmortizareRecomandat + "]";
	}

}
