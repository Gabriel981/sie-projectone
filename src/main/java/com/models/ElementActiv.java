package com.models;

import java.util.Date;

public class ElementActiv {
	private Integer iElementActivNo;
	private String sDenumireElementActiv;
	private String sAliasElementActiv;
	private String sDescriereElementActiv;
	private Date dtDataIntrareGestiune;
	private Date dtDataIesireGestiune;
	private Double dCostIstoric;
	private Double dValoareRecuperare;
	private Integer iDurataUtilizare;
	private Amortizare amortizareAplicata;

	public ElementActiv(Integer iElementActivNo, String sDenumireElementActiv, String sAliasElementActiv,
			String sDescriereElementActiv, Date dtDataIntrareGestiune, Date dtDataIesireGestiune, Double dCostIstoric,
			Double dValoareRecuperare, Integer iDurataUtilizare) {
		super();
		this.iElementActivNo = iElementActivNo;
		this.sDenumireElementActiv = sDenumireElementActiv;
		this.sAliasElementActiv = sAliasElementActiv;
		this.sDescriereElementActiv = sDescriereElementActiv;
		this.dtDataIntrareGestiune = dtDataIntrareGestiune;
		this.dtDataIesireGestiune = dtDataIesireGestiune;
		this.dCostIstoric = dCostIstoric;
		this.dValoareRecuperare = dValoareRecuperare;
		this.iDurataUtilizare = iDurataUtilizare;
	}

	public Integer getiElementActivNo() {
		return iElementActivNo;
	}

	public void setiElementActivNo(Integer iElementActivNo) {
		this.iElementActivNo = iElementActivNo;
	}

	public String getsDenumireElementActiv() {
		return sDenumireElementActiv;
	}

	public void setsDenumireElementActiv(String sDenumireElementActiv) {
		this.sDenumireElementActiv = sDenumireElementActiv;
	}

	public String getsAliasElementActiv() {
		return sAliasElementActiv;
	}

	public void setsAliasElementActiv(String sAliasElementActiv) {
		this.sAliasElementActiv = sAliasElementActiv;
	}

	public String getsDescriereElementActiv() {
		return sDescriereElementActiv;
	}

	public void setsDescriereElementActiv(String sDescriereElementActiv) {
		this.sDescriereElementActiv = sDescriereElementActiv;
	}

	public Date getDtDataIntrareGestiune() {
		return dtDataIntrareGestiune;
	}

	public void setDtDataIntrareGestiune(Date dtDataIntrareGestiune) {
		this.dtDataIntrareGestiune = dtDataIntrareGestiune;
	}

	public Date getDtDataIesireGestiune() {
		return dtDataIesireGestiune;
	}

	public void setDtDataIesireGestiune(Date dtDataIesireGestiune) {
		this.dtDataIesireGestiune = dtDataIesireGestiune;
	}

	public Amortizare getAmortizareAplicata() {
		return amortizareAplicata;
	}

	public void setAmortizareAplicata(Amortizare amortizareAplicata) {
		this.amortizareAplicata = amortizareAplicata;
	}

	public Double getdCostIstoric() {
		return dCostIstoric;
	}

	public void setdCostIstoric(Double dCostIstoric) {
		this.dCostIstoric = dCostIstoric;
	}

	public Double getdValoareRecuperare() {
		return dValoareRecuperare;
	}

	public void setdValoareRecuperare(Double dValoareRecuperare) {
		this.dValoareRecuperare = dValoareRecuperare;
	}

	public Integer getiDurataUtilizare() {
		return iDurataUtilizare;
	}

	public void setiDurataUtilizare(Integer iDurataUtilizare) {
		this.iDurataUtilizare = iDurataUtilizare;
	}
	
	
}
