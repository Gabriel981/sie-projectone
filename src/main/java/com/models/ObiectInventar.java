package com.models;

import java.util.Date;

public class ObiectInventar extends ElementActiv {
	
	private Double dValoareCumparare;
	
	public ObiectInventar(Integer iElementActivNo, String sDenumireElementActiv, String sAliasElementActiv,
			String sDescriereElementActiv, Date dtDataIntrareGestiune, Date dtDataIesireGestiune, Double dCostIstoric,
			Double dValoareRecuperare, Integer iDurataUtilizare, Double dValoareCumparare) {
		super(iElementActivNo, sDenumireElementActiv, sAliasElementActiv, sDescriereElementActiv, dtDataIntrareGestiune,
				dtDataIesireGestiune, dCostIstoric, dValoareRecuperare, iDurataUtilizare);
		this.dValoareCumparare = dValoareCumparare;
	}

	public Double getdValoareCumparare() {
		return dValoareCumparare;
	}

	public void setdValoareCumparare(Double dValoareCumparare) {
		this.dValoareCumparare = dValoareCumparare;
	}
}
