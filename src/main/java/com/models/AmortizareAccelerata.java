package com.models;

import java.util.ArrayList;
import java.util.List;

public class AmortizareAccelerata extends Amortizare {
	private Double dValoareIntrareMijlocFix;
	private Double dRataAmortizare;
	private Integer iNumarAniAmortizare;
	private Double dValoareCalculataAmortizare;
	private List<IstoricAmortizare> istoricAmortizare;
		
	public AmortizareAccelerata(TipAmortizare eTipAmortizare, Double dValoareIntrareMijlocFix, Double dRataAmortizare,
			Integer iNumarAniAmortizare) {
		super(eTipAmortizare);
		this.dValoareIntrareMijlocFix = dValoareIntrareMijlocFix;
		this.dRataAmortizare = dRataAmortizare;
		this.iNumarAniAmortizare = iNumarAniAmortizare;
		this.istoricAmortizare = new ArrayList<IstoricAmortizare>();
	}

	public Double getdValoareIntrareMijlocFix() {
		return dValoareIntrareMijlocFix;
	}

	public void setdValoareIntrareMijlocFix(Double dValoareIntrareMijlocFix) {
		this.dValoareIntrareMijlocFix = dValoareIntrareMijlocFix;
	}

	public Double getdRataAmortizare() {
		return dRataAmortizare;
	}

	public void setdRataAmortizare(Double dRataAmortizare) {
		this.dRataAmortizare = dRataAmortizare;
	}

	public Integer getiNumarAniAmortizare() {
		return iNumarAniAmortizare;
	}

	public void setiNumarAniAmortizare(Integer iNumarAniAmortizare) {
		this.iNumarAniAmortizare = iNumarAniAmortizare;
	}

	public Double getdValoareCalculataAmortizare() {
		return dValoareCalculataAmortizare;
	}
	
	public void setdValoareCalculataAmortizare(Double dValoareCalculataAmortizare) {
		this.dValoareCalculataAmortizare = dValoareCalculataAmortizare;
	}

	public List<IstoricAmortizare> getIstoricAmortizare() {
		return istoricAmortizare;
	}
	
	
}
