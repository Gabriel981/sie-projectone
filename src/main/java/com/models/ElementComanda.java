package com.models;

public class ElementComanda {
	private Integer iElementComandaNumar;
	private String sDenumireElementComanda;
	private String sDescriereElementComanda;

	public ElementComanda(Integer iElementComandaNumar, String sDenumireElementComanda,
			String sDescriereElementComanda) {
		super();
		this.iElementComandaNumar = iElementComandaNumar;
		this.sDenumireElementComanda = sDenumireElementComanda;
		this.sDescriereElementComanda = sDescriereElementComanda;
	}

	public Integer getiElementComandaNumar() {
		return iElementComandaNumar;
	}

	public void setiElementComandaNumar(Integer iElementComandaNumar) {
		this.iElementComandaNumar = iElementComandaNumar;
	}

	public String getsDenumireElementComanda() {
		return sDenumireElementComanda;
	}

	public void setsDenumireElementComanda(String sDenumireElementComanda) {
		this.sDenumireElementComanda = sDenumireElementComanda;
	}

	public String getsDescriereElementComanda() {
		return sDescriereElementComanda;
	}

	public void setsDescriereElementComanda(String sDescriereElementComanda) {
		this.sDescriereElementComanda = sDescriereElementComanda;
	}

	
}
