package com.models;

import java.util.Date;

public class MijlocFix extends ElementActiv {

	private TipIncadrareGrupa eIncadrareGrupa;
	private TipAmortizare tipAmortizareAles;
	
	public MijlocFix(Integer iElementActivNo, String sDenumireElementActiv, String sAliasElementActiv,
			String sDescriereElementActiv, Date dtDataIntrareGestiune, Date dtDataIesireGestiune, Double dCostIstoric,
			Double dValoareRecuperare, Integer iDurataUtilizare, TipIncadrareGrupa eIncadrareGrupa, TipAmortizare tipAmortizare) {
		super(iElementActivNo, sDenumireElementActiv, sAliasElementActiv, sDescriereElementActiv, dtDataIntrareGestiune,
				dtDataIesireGestiune, dCostIstoric, dValoareRecuperare, iDurataUtilizare);
		this.eIncadrareGrupa = eIncadrareGrupa;
		
		if(this.geteIncadrareGrupa() == TipIncadrareGrupa.CONSTRUCTII) {
			this.setAmortizareAplicata(new AmortizareLiniara(TipAmortizare.LINIARA, this.getdCostIstoric(), this.getdValoareRecuperare(), this.getiDurataUtilizare()));
			this.getAmortizareAplicata().setElementActiv(this);
		}
		
		if(tipAmortizare == TipAmortizare.DEGRESIVA) {
			this.setAmortizareAplicata(new AmortizareDegresiva(tipAmortizare, this.getdCostIstoric(), this.getiDurataUtilizare()));
			this.getAmortizareAplicata().setElementActiv(this);
		}
		
		if(tipAmortizare == TipAmortizare.ACCELERATA) {
			this.setAmortizareAplicata(new AmortizareAccelerata(tipAmortizare, this.getdCostIstoric(), null, this.getiDurataUtilizare()));
			this.getAmortizareAplicata().setElementActiv(this);
		}
	}

	public TipIncadrareGrupa geteIncadrareGrupa() {
		return eIncadrareGrupa;
	}

	public void seteIncadrareGrupa(TipIncadrareGrupa eIncadrareGrupa) {
		this.eIncadrareGrupa = eIncadrareGrupa;
	}

	public TipAmortizare getTipAmortizareAles() {
		return tipAmortizareAles;
	}

	public void setTipAmortizareAles(TipAmortizare tipAmortizareAles) {
		this.tipAmortizareAles = tipAmortizareAles;
	}

	@Override
	public String toString() {
		return "MijlocFix [eIncadrareGrupa=" + eIncadrareGrupa + ", tipAmortizareAles=" + tipAmortizareAles
				+ ", getiElementActivNo()=" + getiElementActivNo() + ", getsDenumireElementActiv()="
				+ getsDenumireElementActiv() + ", getsAliasElementActiv()=" + getsAliasElementActiv()
				+ ", getsDescriereElementActiv()=" + getsDescriereElementActiv() + ", getDtDataIntrareGestiune()="
				+ getDtDataIntrareGestiune() + ", getDtDataIesireGestiune()=" + getDtDataIesireGestiune()
				+ ", getAmortizareAplicata()=" + getAmortizareAplicata() + ", getdCostIstoric()=" + getdCostIstoric()
				+ ", getdValoareRecuperare()=" + getdValoareRecuperare() + ", getiDurataUtilizare()="
				+ getiDurataUtilizare() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	
}
