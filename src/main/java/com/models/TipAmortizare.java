package com.models;

public enum TipAmortizare {
	LINIARA, ACCELERATA, DEGRESIVA;
}
