package com.models;

import java.util.ArrayList;
import java.util.List;

public class AmortizareDegresiva extends Amortizare {
	private Double dValoareIntrareMijlocFix;
	private Integer iNumarAniAmortizare;
	private Integer iCotaAmortizare;
	private Double dValoareCalculataAmortizare;
	private List<IstoricAmortizare> istoricAmortizare;
	
	public AmortizareDegresiva(TipAmortizare eTipAmortizare, Double dValoareIntrareMijlocFix,
			Integer iNumarAniAmortizare) {
		super(eTipAmortizare);
		this.dValoareIntrareMijlocFix = dValoareIntrareMijlocFix;
		this.iNumarAniAmortizare = iNumarAniAmortizare;
		
		if(this.iNumarAniAmortizare != null) {
			this.iCotaAmortizare = 100 / iNumarAniAmortizare;
		}
		
		this.istoricAmortizare = new ArrayList<IstoricAmortizare>();
	}

	public Double getdValoareIntrareMijlocFix() {
		return dValoareIntrareMijlocFix;
	}

	public void setdValoareIntrareMijlocFix(Double dValoareIntrareMijlocFix) {
		this.dValoareIntrareMijlocFix = dValoareIntrareMijlocFix;
	}

	public Integer getiNumarAniAmortizare() {
		return iNumarAniAmortizare;
	}

	public void setiNumarAniAmortizare(Integer iNumarAniAmortizare) {
		this.iNumarAniAmortizare = iNumarAniAmortizare;
	}

	public Integer getiCotaAmortizare() {
		return iCotaAmortizare;
	}

	public void setiCotaAmortizare(Integer iCotaAmortizare) {
		this.iCotaAmortizare = iCotaAmortizare;
	}

	public Double getdValoareCalculataAmortizare() {
		return dValoareCalculataAmortizare;
	}
	
	public void setdValoareCalculataAmortizare(Double dValoareCalculataAmortizare) {
		this.dValoareCalculataAmortizare = dValoareCalculataAmortizare;
	}

	public List<IstoricAmortizare> getIstoricAmortizare() {
		return istoricAmortizare;
	}
	
	
}
