package com.models;

import java.util.ArrayList;
import java.util.List;

public class AmortizareLiniara extends Amortizare {
	private Double dCostIstoric;
	private Double dValoareRecuperare;
	private Double dValoareAmortizare;
	private Integer iNumarAniAmortizare;
	private Double dRataAmortizare;
	private Double dValoareCalculataAmortizare;
	private List<IstoricAmortizare> istoricAmortizari;
	
	public AmortizareLiniara(TipAmortizare eTipAmortizare, Double dCostIstoric, Double dValoareRecuperare, Integer iNumarAniAmortizare) {
		super(eTipAmortizare);
		this.dCostIstoric = dCostIstoric;
		this.dValoareRecuperare = dValoareRecuperare;
		this.iNumarAniAmortizare = iNumarAniAmortizare;
		this.istoricAmortizari = new ArrayList<>();
	}

	public Double getdCostIstoric() {
		return dCostIstoric;
	}

	public void setdCostIstoric(Double dCostIstoric) {
		this.dCostIstoric = dCostIstoric;
	}

	public Double getdValoareRecuperare() {
		return dValoareRecuperare;
	}

	public void setdValoareRecuperare(Double dValoareRecuperare) {
		this.dValoareRecuperare = dValoareRecuperare;
	}

	public Double getdValoareAmortizare() {
		return dValoareAmortizare;
	}

	public void setdValoareAmortizare(Double dValoareAmortizare) {
		this.dValoareAmortizare = dValoareAmortizare;
	}

	public Integer getiNumarAniAmortizare() {
		return iNumarAniAmortizare;
	}

	public void setiNumarAniAmortizare(Integer iNumarAniAmortizare) {
		this.iNumarAniAmortizare = iNumarAniAmortizare;
	}

	public Double getdValoareCalculataAmortizare() {
		return dValoareCalculataAmortizare;
	}

	public void setdValoareCalculataAmortizare(Double dValoareCalculataAmortizare) {
		this.dValoareCalculataAmortizare = dValoareCalculataAmortizare;
	}

	public Double getdRataAmortizare() {
		return dRataAmortizare;
	}
	
	public void setdRataAmortizare(Double dRataAmortizare) {
		this.dRataAmortizare = dRataAmortizare;
	}

	public List<IstoricAmortizare> getIstoricAmortizari() {
		return istoricAmortizari;
	}

	@Override
	public String toString() {
		return "AmortizareLiniara [dCostIstoric=" + dCostIstoric + ", dValoareRecuperare=" + dValoareRecuperare
				+ ", dValoareAmortizare=" + dValoareAmortizare + ", iNumarAniAmortizare=" + iNumarAniAmortizare
				+ ", dRataAmortizare=" + dRataAmortizare + ", dValoareCalculataAmortizare="
				+ dValoareCalculataAmortizare + ", istoricAmortizari=" + istoricAmortizari + ", getsAmortizareId()="
				+ getsAmortizareId() + ", geteTipAmortizare()=" + geteTipAmortizare() + ", getdValoareAmortizata()="
				+ getdValoareAmortizata() + ", getElementActiv()=" + getElementActiv() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
}
