package com.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comanda {
	private Integer iComandaNumar;
	private Date dtOperationalizareComanda;
	private Double dValoareComanda;
	private List<LinieComanda> liniiComanda;
	
	public Comanda(Integer iComandaNumar, Date dtOperationalizareComanda) {
		super();
		this.iComandaNumar = iComandaNumar;
		this.dtOperationalizareComanda = dtOperationalizareComanda;
		this.dValoareComanda = 0.00;
		this.liniiComanda = new ArrayList<LinieComanda>();
	}

	public Integer getiComandaNumar() {
		return iComandaNumar;
	}

	public void setiComandaNumar(Integer iComandaNumar) {
		this.iComandaNumar = iComandaNumar;
	}

	public Date getDtOperationalizareComanda() {
		return dtOperationalizareComanda;
	}

	public void setDtOperationalizareComanda(Date dtOperationalizareComanda) {
		this.dtOperationalizareComanda = dtOperationalizareComanda;
	}

	public Double getdValoareComanda() {
		return dValoareComanda;
	}
	
	public List<LinieComanda> getLiniiComanda() {
		return liniiComanda;
	}
	
	public void updateValoareComanda(Double valoareNoua) 
	{
		if(valoareNoua >= 0 && !valoareNoua.equals(null)) {
			this.dValoareComanda += valoareNoua;
		}
	}
}
