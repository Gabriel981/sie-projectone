package com.models;

import com.viewModels.CreareElementActivViewModel;

public class LinieComanda {
	private Integer iLinieComandaNumar;
	private ElementComanda elementComanda;
	private Integer iCantitateComanda;
	private Double dValoareUnitara;
	private String sMentiuniLinieComanda;
	
	public LinieComanda(Integer iLinieComandaNumar, ElementComanda elementComanda, Integer iCantitateComanda,
			Double dValoareUnitara, String sMentiuniLinieComanda) {
		super();
		this.iLinieComandaNumar = iLinieComandaNumar;
		this.elementComanda = elementComanda;
		this.iCantitateComanda = iCantitateComanda;
		this.dValoareUnitara = dValoareUnitara;
		this.sMentiuniLinieComanda = sMentiuniLinieComanda;
	}

	public Integer getiLinieComandaNumar() {
		return iLinieComandaNumar;
	}

	public void setiLinieComandaNumar(Integer iLinieComandaNumar) {
		this.iLinieComandaNumar = iLinieComandaNumar;
	}

	public ElementComanda getElementComanda() {
		return elementComanda;
	}

	public void setElementComanda(ElementComanda elementComanda) {
		this.elementComanda = elementComanda;
	}

	public Integer getiCantitateComanda() {
		return iCantitateComanda;
	}

	public void setiCantitateComanda(Integer iCantitateComanda) {
		this.iCantitateComanda = iCantitateComanda;
	}

	public Double getdValoareUnitara() {
		return dValoareUnitara;
	}

	public void setdValoareUnitara(Double dValoareUnitara) {
		this.dValoareUnitara = dValoareUnitara;
	}

	public String getsMentiuniLinieComanda() {
		return sMentiuniLinieComanda;
	}

	public void setsMentiuniLinieComanda(String sMentiuniLinieComanda) {
		this.sMentiuniLinieComanda = sMentiuniLinieComanda;
	}
	
	public Object incadrareElementActiv(CreareElementActivViewModel viewModel) {
		if(viewModel != null) {
			if(dValoareUnitara < 2500.0) {
				return new ObiectInventar(viewModel.getiElementActivNo(), viewModel.getsDenumireElementActiv(), viewModel.getsAliasElementActiv(), viewModel.getsDescriereElementActiv(), viewModel.getDtDataIntrareGestiune(), viewModel.getDtDataIesireGestiune(), viewModel.getdValoareIntrare(), viewModel.getdValoareRecuperare(), viewModel.getiDurataUtilizare(), viewModel.getdValoareIntrare());
			} else {
				return new MijlocFix(viewModel.getiElementActivNo(), viewModel.getsDenumireElementActiv(), viewModel.getsAliasElementActiv(), viewModel.getsDescriereElementActiv(), viewModel.getDtDataIntrareGestiune(), viewModel.getDtDataIesireGestiune(), viewModel.getdValoareIntrare(), viewModel.getdValoareRecuperare(), viewModel.getiDurataUtilizare(), viewModel.geteIncadrareGrupa(), viewModel.getTipAmortizareAleasa());
			}
		}
		
		return null;
	}
}
