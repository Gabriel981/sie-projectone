package com.models;

public class IstoricAmortizare {
	private Integer iIstoricAmortizareNumar;
	private Double dValoareAmortizareRezultata;
	private Integer iAnRealizareAmortizare;
	private Amortizare amortizare;
	
	public IstoricAmortizare(Integer iIstoricAmortizareNumar, Double dValoareAmortizareRezultata,
			Integer iAnRealizareAmortizare, Amortizare amortizare) {
		super();
		this.iIstoricAmortizareNumar = iIstoricAmortizareNumar;
		this.dValoareAmortizareRezultata = dValoareAmortizareRezultata;
		this.iAnRealizareAmortizare = iAnRealizareAmortizare;
		this.amortizare = amortizare;
	}

	public Integer getiIstoricAmortizareNumar() {
		return iIstoricAmortizareNumar;
	}

	public void setiIstoricAmortizareNumar(Integer iIstoricAmortizareNumar) {
		this.iIstoricAmortizareNumar = iIstoricAmortizareNumar;
	}

	public Double getdValoareAmortizareRezultata() {
		return dValoareAmortizareRezultata;
	}

	public void setdValoareAmortizareRezultata(Double dValoareAmortizareRezultata) {
		this.dValoareAmortizareRezultata = dValoareAmortizareRezultata;
	}

	public Integer getiAnRealizareAmortizare() {
		return iAnRealizareAmortizare;
	}

	public void setiAnRealizareAmortizare(Integer iAnRealizareAmortizare) {
		this.iAnRealizareAmortizare = iAnRealizareAmortizare;
	}

	public Amortizare getAmortizare() {
		return amortizare;
	}

	public void setAmortizare(Amortizare amortizare) {
		this.amortizare = amortizare;
	}
	
	
}
