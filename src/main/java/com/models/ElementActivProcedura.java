package com.models;

import java.util.UUID;

public class ElementActivProcedura {
	private String sElementActivProceduraNo;
	private String sDenumireElementActiv;
	private String sAliasElementActiv;
	private String sDescriereElementActiv;
	private Integer iDurataUtilizare;
	private TipIncadrareGrupa eIncadrareGrupa;
	
	public ElementActivProcedura(String sDenumireElementActiv, String sAliasElementActiv, String sDescriereElementActiv,
			Integer iDurataUtilizare, TipIncadrareGrupa eIncadrareGrupa) {
		super();
		this.sDenumireElementActiv = sDenumireElementActiv;
		this.sAliasElementActiv = sAliasElementActiv;
		this.sDescriereElementActiv = sDescriereElementActiv;
		this.iDurataUtilizare = iDurataUtilizare;
		this.eIncadrareGrupa = eIncadrareGrupa;
		this.setsElementActivProceduraNo(UUID.randomUUID().toString());
	}

	public String getsElementActivProceduraNo() {
		return sElementActivProceduraNo;
	}

	public void setsElementActivProceduraNo(String sElementActivProceduraNo) {
		this.sElementActivProceduraNo = sElementActivProceduraNo;
	}

	public String getsDenumireElementActiv() {
		return sDenumireElementActiv;
	}

	public void setsDenumireElementActiv(String sDenumireElementActiv) {
		this.sDenumireElementActiv = sDenumireElementActiv;
	}

	public String getsAliasElementActiv() {
		return sAliasElementActiv;
	}

	public void setsAliasElementActiv(String sAliasElementActiv) {
		this.sAliasElementActiv = sAliasElementActiv;
	}

	public String getsDescriereElementActiv() {
		return sDescriereElementActiv;
	}

	public void setsDescriereElementActiv(String sDescriereElementActiv) {
		this.sDescriereElementActiv = sDescriereElementActiv;
	}

	public Integer getiDurataUtilizare() {
		return iDurataUtilizare;
	}

	public void setiDurataUtilizare(Integer iDurataUtilizare) {
		this.iDurataUtilizare = iDurataUtilizare;
	}

	public TipIncadrareGrupa geteIncadrareGrupa() {
		return eIncadrareGrupa;
	}

	public void seteIncadrareGrupa(TipIncadrareGrupa eIncadrareGrupa) {
		this.eIncadrareGrupa = eIncadrareGrupa;
	}
	
	
}
