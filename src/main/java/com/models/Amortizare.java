package com.models;

import java.util.UUID;

public class Amortizare {

	private String sAmortizareId;
	private TipAmortizare eTipAmortizare;
	private Double dValoareAmortizata;
	private ElementActiv elementActiv;
	
	public Amortizare(TipAmortizare eTipAmortizare) {
		super();
		this.eTipAmortizare = eTipAmortizare;
		
		this.setsAmortizareId(UUID.randomUUID().toString());
	}

	public String getsAmortizareId() {
		return sAmortizareId;
	}

	public void setsAmortizareId(String sAmortizareId) {
		this.sAmortizareId = sAmortizareId;
	}


	public TipAmortizare geteTipAmortizare() {
		return eTipAmortizare;
	}

	public void seteTipAmortizare(TipAmortizare eTipAmortizare) {
		this.eTipAmortizare = eTipAmortizare;
	}

	public Double getdValoareAmortizata() {
		return dValoareAmortizata;
	}

	public ElementActiv getElementActiv() {
		return elementActiv;
	}

	public void setElementActiv(ElementActiv elementActiv) {
		this.elementActiv = elementActiv;
	}

	@Override
	public String toString() {
		return "Amortizare [sAmortizareId=" + sAmortizareId + ", eTipAmortizare=" + eTipAmortizare
				+ ", dValoareAmortizata=" + dValoareAmortizata + ", elementActiv=" + elementActiv + "]";
	}

	
}
