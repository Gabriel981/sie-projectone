package com.tests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.helpers.KnowledgeSessionHelper;
import com.models.Comanda;
import com.models.ElementActivProcedura;
import com.models.ElementComanda;
import com.models.LinieComanda;
import com.models.MijlocFix;
import com.models.ProceduraContabilaAmortizare;
import com.models.TipAmortizare;
import com.models.TipIncadrareGrupa;
import com.operational.IOperatiuniCalculAmortizare;
import com.operational.IOperatiuniProceduriContabile;
import com.operational.OperatiuniCalculAmortizare;
import com.operational.OperatiuniComanda;
import com.operational.OperatiuniProceduriContabile;
import com.operational.iOperatiuniComanda;
import com.viewModels.CreareElementActivViewModel;
import com.viewModels.TipAmortizareViewModel;

public class DroolsTestTwo {

	public static void main(String[] args) {
		KieServices kieService = KieServices.Factory.get();
 	    KieContainer kieContainer = kieService.getKieClasspathContainer();
     	KieSession kieSession = KnowledgeSessionHelper.getStatefulKnowledgeSession(kieContainer, "ksession-rules-two");
     	kieSession.getAgenda().getAgendaGroup("second rules").setFocus();
     	
     	//Interface that stores all the formulas and methods to get the depreciations based on the paramaters provided
     	IOperatiuniCalculAmortizare operatiuniCalculAmortizare = new OperatiuniCalculAmortizare();
     	
     	IOperatiuniProceduriContabile operatiuniProceduriContabile = new OperatiuniProceduriContabile();
     	
     	//Interfaces that stores all the methods for storing assets in a "COMANDA" object
     	iOperatiuniComanda operatiuniComanda = new OperatiuniComanda();
     	
     	//Set global variable, that can be used in rules.drl files
     	kieSession.setGlobal("operatiuniCalculAmortizare", operatiuniCalculAmortizare);
     	
     	List<ElementActivProcedura> jurnalElementeActivProcedura = new ArrayList<ElementActivProcedura>();
     	ElementActivProcedura eapOne = new ElementActivProcedura("Laptop", "LAP", "Descriere laptop", 4, TipIncadrareGrupa.MOBILIER);
     	ElementActivProcedura eapTwo = new ElementActivProcedura("Excavator", "EXC", "Descriere excavator", 10, TipIncadrareGrupa.CONSTRUCTII);
     	ElementActivProcedura eapThree = new ElementActivProcedura("Birou", "BIR", "Descriere birou", 3, TipIncadrareGrupa.MOBILIER);
     	
     	jurnalElementeActivProcedura.add(eapOne);
     	jurnalElementeActivProcedura.add(eapTwo);
     	jurnalElementeActivProcedura.add(eapThree);
     	
     	List<ProceduraContabilaAmortizare> proceduriContabileRecomandabile = new ArrayList<ProceduraContabilaAmortizare>();
     	if(jurnalElementeActivProcedura != null && jurnalElementeActivProcedura.size() > 0) {
     		for(ElementActivProcedura elementActivProcedura : jurnalElementeActivProcedura) {
     			if(elementActivProcedura != null) {
     				if(elementActivProcedura.geteIncadrareGrupa() == TipIncadrareGrupa.CONSTRUCTII) {
         				proceduriContabileRecomandabile.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.LINIARA));
     				}
     				else {
     					proceduriContabileRecomandabile.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.DEGRESIVA));
     				}	
     			}
     		}
     	}
     
     	
     	//First object of type Comanda, that will start the flow for rule no. 1, second file
     	//Rule no. 1 (second file): Alegerea tipului de amortizare degresiva pentru un mijloc fix, conform caietului de proceduri contabile
     	Comanda comandaProceduriContabileOne = new Comanda(1, new Date());
     	ElementComanda elementComandaProceduraOne = new ElementComanda(1, "Laptop", "Test laptop descriere");
     	LinieComanda linieComandaProceduraOne = new LinieComanda(1, elementComandaProceduraOne, 1, 4000.0, "Nu exista mentiuni");
     	CreareElementActivViewModel creareElementProceduraOne = new CreareElementActivViewModel(
     			1,
     			elementComandaProceduraOne.getsDenumireElementComanda(),
     			elementComandaProceduraOne.getsDenumireElementComanda().toUpperCase().substring(0, 3),
     			elementComandaProceduraOne.getsDescriereElementComanda(),
     			comandaProceduriContabileOne.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaProceduraOne.getdValoareUnitara(),
     			0.0,
     			4,
     			null);
     	
     	operatiuniComanda.addLinieComanda(comandaProceduriContabileOne, linieComandaProceduraOne, creareElementProceduraOne);
     	if(linieComandaProceduraOne.incadrareElementActiv(creareElementProceduraOne) instanceof MijlocFix) {
     		MijlocFix mijlocFix = (MijlocFix) linieComandaProceduraOne.incadrareElementActiv(creareElementProceduraOne);
     		TipAmortizareViewModel returnedModel = operatiuniProceduriContabile.verificaMetodaAmortizareProcedura(proceduriContabileRecomandabile, mijlocFix, creareElementProceduraOne);
     		mijlocFix.setTipAmortizareAles(returnedModel.getTipAmortizareAlocat());
         	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     		kieSession.insert(mijlocFix);
     		if(returnedModel != null && returnedModel.getTipAmortizareAlocat() != null && returnedModel.getAmortizareAlocata() != null) {
     			kieSession.insert(returnedModel);
     		}
     	}
     	
     	List<ElementActivProcedura> jurnalElementeActivProceduraTwo = new ArrayList<ElementActivProcedura>();
     	ElementActivProcedura eapOneV2 = new ElementActivProcedura("Laptop", "LAP", "Descriere laptop", 4, TipIncadrareGrupa.MOBILIER);
     	ElementActivProcedura eapTwoV2 = new ElementActivProcedura("Excavator", "EXC", "Descriere excavator", 10, TipIncadrareGrupa.CONSTRUCTII);
     	ElementActivProcedura eapThreeV2 = new ElementActivProcedura("Birou", "BIR", "Descriere birou", 3, TipIncadrareGrupa.MOBILIER);
     	
     	jurnalElementeActivProceduraTwo.add(eapOneV2);
     	jurnalElementeActivProceduraTwo.add(eapTwoV2);
     	jurnalElementeActivProceduraTwo.add(eapThreeV2);
     	
     	List<ProceduraContabilaAmortizare> proceduriContabileRecomandabileTwo = new ArrayList<ProceduraContabilaAmortizare>();
     	if(jurnalElementeActivProceduraTwo != null && jurnalElementeActivProceduraTwo.size() > 0) {
     		for(ElementActivProcedura elementActivProcedura : jurnalElementeActivProceduraTwo) {
     			if(elementActivProcedura.geteIncadrareGrupa() == TipIncadrareGrupa.CONSTRUCTII) {
     				proceduriContabileRecomandabileTwo.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.LINIARA));
 				}
 				else {
 					proceduriContabileRecomandabileTwo.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.LINIARA));
 				}	
     		}
     	}
     
     	
     	//Second object of type Comanda, that will start the flow for rule no. 2
     	//Rule no. 2 (second file): Alegerea tipului de amortizare liniara pentru un mijloc fix conform caietului de proceduri contabile
     	Comanda comandaProceduriContabileTwo = new Comanda(2, new Date());
     	ElementComanda elementComandaProceduraTwo = new ElementComanda(2, "Birou", "Test birou descriere");
     	LinieComanda linieComandaProceduraTwo = new LinieComanda(2, elementComandaProceduraTwo, 1, 4000.0, "Nu exista mentiuni");
     	CreareElementActivViewModel creareElementProceduraTwo = new CreareElementActivViewModel(
     			2,
     			elementComandaProceduraTwo.getsDenumireElementComanda(),
     			elementComandaProceduraTwo.getsDenumireElementComanda().toUpperCase().substring(0, 3),
     			elementComandaProceduraTwo.getsDescriereElementComanda(),
     			comandaProceduriContabileTwo.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaProceduraTwo.getdValoareUnitara(),
     			0.0,
     			4,
     			null);
     	
     	operatiuniComanda.addLinieComanda(comandaProceduriContabileTwo, linieComandaProceduraTwo, creareElementProceduraTwo);
     	if(linieComandaProceduraTwo.incadrareElementActiv(creareElementProceduraTwo) instanceof MijlocFix) {
     		MijlocFix mijlocFixTwo = (MijlocFix) linieComandaProceduraTwo.incadrareElementActiv(creareElementProceduraTwo);
     		TipAmortizareViewModel returnedModelTwo = operatiuniProceduriContabile.verificaMetodaAmortizareProcedura(proceduriContabileRecomandabileTwo, mijlocFixTwo, creareElementProceduraTwo);
     		mijlocFixTwo.setTipAmortizareAles(returnedModelTwo.getTipAmortizareAlocat());
         	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     		kieSession.insert(mijlocFixTwo);
     		if(returnedModelTwo != null && returnedModelTwo.getTipAmortizareAlocat() != null && returnedModelTwo.getAmortizareAlocata() != null) {
     			kieSession.insert(returnedModelTwo);
     		}
     	}
     	
     	//rule no. 3
     	List<ProceduraContabilaAmortizare> proceduriContabileRecomandabileThree = new ArrayList<ProceduraContabilaAmortizare>();
     	if(jurnalElementeActivProceduraTwo != null && jurnalElementeActivProceduraTwo.size() > 0) {
     		for(ElementActivProcedura elementActivProcedura : jurnalElementeActivProceduraTwo) {
     			if(elementActivProcedura.geteIncadrareGrupa() == TipIncadrareGrupa.CONSTRUCTII) {
     				proceduriContabileRecomandabileThree.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.LINIARA));
 				}
 				else {
 					proceduriContabileRecomandabileThree.add(new ProceduraContabilaAmortizare("Amortizare obiect", new Date(), elementActivProcedura, TipAmortizare.ACCELERATA));
 				}	
     		}
     	}
     	
     	//Third object of type Comanda, that will start the flow for rule no. 3
     	//Rule no. 3 (second file): Alegerea ratei pentru amortizare degresiva la un bun cu durata de viata intre 6-10 ani
     	Comanda comandaProceduriContabileThree= new Comanda(3, new Date());
     	ElementComanda elementComandaProceduraThree = new ElementComanda(3, "Birou", "Test birou descriere");
     	LinieComanda linieComandaProceduraThree = new LinieComanda(3, elementComandaProceduraThree, 1, 4000.0, "Nu exista mentiuni");
     	CreareElementActivViewModel creareElementProceduraThree = new CreareElementActivViewModel(
     			3,
     			elementComandaProceduraThree.getsDenumireElementComanda(),
     			elementComandaProceduraThree.getsDenumireElementComanda().toUpperCase().substring(0, 3),
     			elementComandaProceduraThree.getsDescriereElementComanda(),
     			comandaProceduriContabileThree.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaProceduraThree.getdValoareUnitara(),
     			0.0,
     			4,
     			null);
     	
     	operatiuniComanda.addLinieComanda(comandaProceduriContabileThree, linieComandaProceduraThree, creareElementProceduraThree);
     	if(linieComandaProceduraThree.incadrareElementActiv(creareElementProceduraThree) instanceof MijlocFix) {
     		MijlocFix mijlocFixThree = (MijlocFix) linieComandaProceduraThree.incadrareElementActiv(creareElementProceduraThree);
     		TipAmortizareViewModel returnedModelThree = operatiuniProceduriContabile.verificaMetodaAmortizareProcedura(proceduriContabileRecomandabileThree, mijlocFixThree, creareElementProceduraThree);
     		mijlocFixThree.setTipAmortizareAles(returnedModelThree.getTipAmortizareAlocat());
     		kieSession.insert(mijlocFixThree);
     		if(returnedModelThree != null && returnedModelThree.getTipAmortizareAlocat() != null && returnedModelThree.getAmortizareAlocata() != null) {
     			kieSession.insert(returnedModelThree);
     		}
     	}
     	

     	kieSession.fireAllRules();
	}

}
