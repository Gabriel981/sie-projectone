package com.tests;

import java.util.Date;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.helpers.KnowledgeSessionHelper;
import com.models.AmortizareAccelerata;
import com.models.Comanda;
import com.models.ElementActiv;
import com.models.ElementComanda;
import com.models.LinieComanda;
import com.models.MijlocFix;
import com.models.TipAmortizare;
import com.models.TipIncadrareGrupa;
import com.operational.IOperatiuniCalculAmortizare;
//import com.operational.IOperatiuniProceduriContabile;
import com.operational.OperatiuniCalculAmortizare;
import com.operational.OperatiuniComanda;
//import com.operational.OperatiuniProceduriContabile;
import com.operational.iOperatiuniComanda;
import com.viewModels.CreareElementActivViewModel;

public class DroolsTest {

	public static void main(String[] args) {
		KieServices kieService = KieServices.Factory.get();
 	    KieContainer kieContainer = kieService.getKieClasspathContainer();
     	KieSession kieSession = KnowledgeSessionHelper.getStatefulKnowledgeSession(kieContainer, "ksession-rules");
     	kieSession.getAgenda().getAgendaGroup("first rules").setFocus();
     	
     	//Interface that stores all the formulas and methods to get the depreciations based on the paramaters provided
     	IOperatiuniCalculAmortizare operatiuniCalculAmortizare = new OperatiuniCalculAmortizare();
     	//IOperatiuniProceduriContabile operatiuniProceduriContabile = new OperatiuniProceduriContabile();
     	
     	//Interfaces that stores all the methods for storing assets in a "COMANDA" object
     	iOperatiuniComanda operatiuniComanda = new OperatiuniComanda();
     	
     	//Set global variable, that can be used in rules.drl files
     	kieSession.setGlobal("operatiuniCalculAmortizare", operatiuniCalculAmortizare);
     	
     	//First object of type Comanda, that will start the flow for rule no. 1
     	//Rule no. 1: Alegerea amortizarii liniare obligatorii pentru grupa CONSTRUCTII a mijloacelor fixe
     	Comanda comandaOne = new Comanda(1, new Date());
     	ElementComanda elementComanda = new ElementComanda(1, "Excavator", "Test excavator descriere");
     	LinieComanda linieComandaOne = new LinieComanda(1, elementComanda, 1, 3500.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivOne = new CreareElementActivViewModel(
     			1,
     			elementComanda.getsDenumireElementComanda(),
     			elementComanda.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComanda.getsDescriereElementComanda(),
     			comandaOne.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.CONSTRUCTII,
     			linieComandaOne.getdValoareUnitara(),
     			1000.0,
     			4,
     			null);
     	
     	operatiuniComanda.addLinieComanda(comandaOne, linieComandaOne, creareElementActivOne);
     	MijlocFix elementActivOne = (MijlocFix) linieComandaOne.incadrareElementActiv(creareElementActivOne);
     	elementActivOne.setTipAmortizareAles(TipAmortizare.LINIARA);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivOne);
     	
     	
     	//Second object of type Comanda, that will start the flow for rule no. 2
     	//Rule no. 2: Alegerea ratei pentru amortizare degresiva la un bun cu durata de viata intre 2-5 ani
     	Comanda comandaTwo = new Comanda(2, new Date());
     	ElementComanda elementComandaTwo = new ElementComanda(2, "Mobilier birou", "Test mobilier birou descriere");
     	LinieComanda linieComandaTwo = new LinieComanda(2, elementComandaTwo, 1, 3000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivTwo = new CreareElementActivViewModel(
     			1,
     			elementComandaTwo.getsDenumireElementComanda(),
     			elementComandaTwo.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaTwo.getsDescriereElementComanda(),
     			comandaTwo.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaTwo.getdValoareUnitara(),
     			0.0,
     			5,
     			TipAmortizare.DEGRESIVA);
     	
     	operatiuniComanda.addLinieComanda(comandaTwo, linieComandaTwo, creareElementActivTwo);
     	MijlocFix elementActivTwo = (MijlocFix) linieComandaTwo.incadrareElementActiv(creareElementActivTwo);
     	elementActivTwo.setTipAmortizareAles(TipAmortizare.DEGRESIVA);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivTwo);
     	
     	//Third object of type Comanda, that will start the flow for rule no. 3
     	//Rule no. 3: Alegerea ratei pentru amortizare degresiva la un bun cu durata de viata intre 6-10 ani
     	Comanda comandaThree = new Comanda(3, new Date());
     	ElementComanda elementComandaThree = new ElementComanda(3, "Bun cu durata de utilizare 6-10 ani", "Test mobilier birou descriere");
     	LinieComanda linieComandaThree = new LinieComanda(3, elementComandaThree, 1, 15000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivThree = new CreareElementActivViewModel(
     			1,
     			elementComandaThree.getsDenumireElementComanda(),
     			elementComandaThree.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaThree.getsDescriereElementComanda(),
     			comandaThree.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.INDUSTRIAL_TRANSPORT_PLANTATII,
     			linieComandaThree.getdValoareUnitara(),
     			0.0,
     			7,
     			TipAmortizare.DEGRESIVA);
     	
     	operatiuniComanda.addLinieComanda(comandaThree, linieComandaThree, creareElementActivThree);
     	MijlocFix elementActivThree = (MijlocFix) linieComandaThree.incadrareElementActiv(creareElementActivThree);
     	elementActivThree.setTipAmortizareAles(TipAmortizare.DEGRESIVA);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivThree);
     	
     	
     	//Fourth object of type Comanda, that will start the flow for rule no. 4
     	//Rule no. 4: Alegerea ratei pentru amortizare degresiva la un bun cu durata de viata de peste 10 ani
     	Comanda comandaFour = new Comanda(4, new Date());
     	ElementComanda elementComandaFour = new ElementComanda(4, "Bun cu durata de utilizare peste 10 ani", "Test mobilier birou descriere");
     	LinieComanda linieComandaFour = new LinieComanda(4, elementComandaFour, 1, 15000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivFour = new CreareElementActivViewModel(
     			1,
     			elementComandaFour.getsDenumireElementComanda(),
     			elementComandaFour.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaFour.getsDescriereElementComanda(),
     			comandaFour.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.INDUSTRIAL_TRANSPORT_PLANTATII,
     			linieComandaFour.getdValoareUnitara(),
     			0.0,
     			15,
     			TipAmortizare.DEGRESIVA);
     	
     	operatiuniComanda.addLinieComanda(comandaFour, linieComandaFour, creareElementActivFour);
     	MijlocFix elementActivFour = (MijlocFix) linieComandaFour.incadrareElementActiv(creareElementActivFour);
     	elementActivFour.setTipAmortizareAles(TipAmortizare.DEGRESIVA);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivFour);
     	
     	//Fifth object of type Comanda, that will start the flow for rule no. 5
     	//Rule no. 5: Incadrarea in procentul de amortizare accelerata, de maxim 0.5 (50%)
     	Comanda comandaFive = new Comanda(5, new Date());
     	ElementComanda elementComandaFive = new ElementComanda(5, "Bun amortizabil", "Test mobilier birou descriere");
     	LinieComanda linieComandaFive = new LinieComanda(5, elementComandaFive, 1, 15000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivFive = new CreareElementActivViewModel(
     			1,
     			elementComandaFive.getsDenumireElementComanda(),
     			elementComandaFive.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaFive.getsDescriereElementComanda(),
     			comandaFive.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.INDUSTRIAL_TRANSPORT_PLANTATII,
     			linieComandaFive.getdValoareUnitara(),
     			0.0,
     			4,
     			TipAmortizare.ACCELERATA);
     	
     	operatiuniComanda.addLinieComanda(comandaFive, linieComandaFive, creareElementActivFive);
     	MijlocFix elementActivFive = (MijlocFix) linieComandaFive.incadrareElementActiv(creareElementActivFive);
     	AmortizareAccelerata amortizareAccelerata = (AmortizareAccelerata) elementActivFive.getAmortizareAplicata();
     	amortizareAccelerata.setdRataAmortizare(0.48);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivFive);
     	//Here the object of type AmortizareAccelerata is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(amortizareAccelerata);
     	
     	
     	//Sixth object of type Comanda, that will start the flow for rule no. 6
     	//Rule no. 6: Nu se incadreaza in procentul de amortizare accelerata pentru primul an, de maxim 0.5 (50%)
     	Comanda comandaSix = new Comanda(6, new Date());
     	ElementComanda elementComandaSix = new ElementComanda(6, "Bun amortizabil", "Test mobilier birou descriere");
     	LinieComanda linieComandaSix = new LinieComanda(6, elementComandaSix, 1, 15000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivSix = new CreareElementActivViewModel(
     			1,
     			elementComandaSix.getsDenumireElementComanda(),
     			elementComandaSix.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaSix.getsDescriereElementComanda(),
     			comandaSix.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.INDUSTRIAL_TRANSPORT_PLANTATII,
     			linieComandaSix.getdValoareUnitara(),
     			0.0,
     			4,
     			TipAmortizare.ACCELERATA);
     	
     	operatiuniComanda.addLinieComanda(comandaSix, linieComandaSix, creareElementActivSix);
     	MijlocFix elementActivSix = (MijlocFix) linieComandaSix.incadrareElementActiv(creareElementActivSix);
     	AmortizareAccelerata amortizareAccelerataTwo = (AmortizareAccelerata) elementActivSix.getAmortizareAplicata();
     	amortizareAccelerataTwo.setdRataAmortizare(0.55);
     	elementActivSix.setTipAmortizareAles(TipAmortizare.ACCELERATA);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivSix);
     	//Here the object of type AmortizareAccelerata is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(amortizareAccelerataTwo);
     	
     	//Seventh object of type Comanda, that will start the flow for rule no. 7
     	//Rule no. 7: Verificare furnizare obiect de inventar
     	Comanda comandaSeven = new Comanda(7, new Date());
     	ElementComanda elementComandaSeven = new ElementComanda(7, "Scaun", "Mobilier de birou");
     	LinieComanda linieComandaSeven = new LinieComanda(7, elementComandaSeven, 1, 200.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivSeven = new CreareElementActivViewModel(
     			1,
     			elementComandaSeven.getsDenumireElementComanda(),
     			elementComandaSeven.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaSeven.getsDescriereElementComanda(),
     			comandaSeven.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaSeven.getdValoareUnitara(),
     			0.0,
     			5,
     			null);
     	operatiuniComanda.addLinieComanda(comandaSeven, linieComandaSeven, creareElementActivSeven);
     	ElementActiv elementActivSeven = (ElementActiv) linieComandaSeven.incadrareElementActiv(creareElementActivSeven);
     	//Here the object of type ElementActiv is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivSeven);
     	
     	//Eigth object of type Comanda, that will start the flow for rule no. 8
     	//Rule no. 8: Selectare amortizare mijloc fix din grupa Alte mijloace fixe
     	Comanda comandaEight = new Comanda(8, new Date());
     	ElementComanda elementComandaEight = new ElementComanda(8, "Birou", "Mobilier de birou");
     	LinieComanda linieComandaEight = new LinieComanda(8, elementComandaEight, 1, 3000.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivEight = new CreareElementActivViewModel(
     			1,
     			elementComandaEight.getsDenumireElementComanda(),
     			elementComandaEight.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaEight.getsDescriereElementComanda(),
     			comandaEight.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.MOBILIER,
     			linieComandaEight.getdValoareUnitara(),
     			0.0,
     			5,
     			null);
     	operatiuniComanda.addLinieComanda(comandaEight, linieComandaEight, creareElementActivEight);
     	ElementActiv elementActivEight = (ElementActiv) linieComandaEight.incadrareElementActiv(creareElementActivEight); 	
     	//Here the object of type ElementActiv is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivEight);
     	
     	//Nineth object of type Comanda, that will start the flow for rule no. 9
     	//Rule no. 9: Verificare furnizare mijloc fix, grupa plantatii, animale si transporturi
     	Comanda comandaNine = new Comanda(1, new Date());
     	ElementComanda elementComandaNine = new ElementComanda(9, "Activ biologic productiv (Cal)", "Activ biologic productiv (Cal)");
     	LinieComanda linieComandaNine = new LinieComanda(9, elementComandaNine, 4, 3500.0, "Nu exista");
     	CreareElementActivViewModel creareElementActivNine = new CreareElementActivViewModel(
     			1,
     			elementComandaNine.getsDenumireElementComanda(),
     			elementComandaNine.getsDenumireElementComanda().toUpperCase().substring(0, 4),
     			elementComandaNine.getsDescriereElementComanda(),
     			comandaNine.getDtOperationalizareComanda(),
     			null,
     			TipIncadrareGrupa.INDUSTRIAL_TRANSPORT_PLANTATII,
     			linieComandaNine.getdValoareUnitara(),
     			1000.0,
     			4,
     			null);
     	
     	operatiuniComanda.addLinieComanda(comandaNine, linieComandaNine, creareElementActivNine);
     	MijlocFix elementActivNine = (MijlocFix) linieComandaNine.incadrareElementActiv(creareElementActivNine);
     	//Here the object of type MijlocFix is pushed to the knowledge base, in order to be processed and also used in rules logic
     	kieSession.insert(elementActivNine);
     	
     	kieSession.fireAllRules();
	}

}
