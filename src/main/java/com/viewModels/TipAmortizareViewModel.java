package com.viewModels;

import com.models.Amortizare;
import com.models.TipAmortizare;

public class TipAmortizareViewModel {
	private TipAmortizare tipAmortizareAlocat;
	private Amortizare amortizareAlocata;
	
	public TipAmortizareViewModel(TipAmortizare tipAmortizareAlocat, Amortizare amortizareAlocata) {
		super();
		this.tipAmortizareAlocat = tipAmortizareAlocat;
		this.amortizareAlocata = amortizareAlocata;
	}

	public TipAmortizare getTipAmortizareAlocat() {
		return tipAmortizareAlocat;
	}

	public void setTipAmortizareAlocat(TipAmortizare tipAmortizareAlocat) {
		this.tipAmortizareAlocat = tipAmortizareAlocat;
	}

	public Amortizare getAmortizareAlocata() {
		return amortizareAlocata;
	}

	public void setAmortizareAlocata(Amortizare amortizareAlocata) {
		this.amortizareAlocata = amortizareAlocata;
	}

	@Override
	public String toString() {
		return "TipAmortizareViewModel [tipAmortizareAlocat=" + tipAmortizareAlocat + ", amortizareAlocata="
				+ amortizareAlocata + "]";
	}
	
	
}
