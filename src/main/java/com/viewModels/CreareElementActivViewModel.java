package com.viewModels;

import java.util.Date;

import com.models.TipAmortizare;
import com.models.TipIncadrareGrupa;

public class CreareElementActivViewModel {
	private Integer iElementActivNo;
	private String sDenumireElementActiv;
	private String sAliasElementActiv;
	private String sDescriereElementActiv;
	private Date dtDataIntrareGestiune;
	private Date dtDataIesireGestiune;
	private TipIncadrareGrupa eIncadrareGrupa;
	private Double dValoareIntrare;
	private Double dValoareRecuperare;
	private Integer iDurataUtilizare;
	private TipAmortizare tipAmortizareAleasa;
	
	
	public CreareElementActivViewModel(Integer iElementActivNo, String sDenumireElementActiv, String sAliasElementActiv,
			String sDescriereElementActiv, Date dtDataIntrareGestiune, Date dtDataIesireGestiune,
			TipIncadrareGrupa eIncadrareGrupa, Double dValoareIntrare, Double dValoareRecuperare,
			Integer iDurataUtilizare, TipAmortizare tipAmortizareAleasa) {
		super();
		this.iElementActivNo = iElementActivNo;
		this.sDenumireElementActiv = sDenumireElementActiv;
		this.sAliasElementActiv = sAliasElementActiv;
		this.sDescriereElementActiv = sDescriereElementActiv;
		this.dtDataIntrareGestiune = dtDataIntrareGestiune;
		this.dtDataIesireGestiune = dtDataIesireGestiune;
		this.eIncadrareGrupa = eIncadrareGrupa;
		this.dValoareIntrare = dValoareIntrare;
		this.dValoareRecuperare = dValoareRecuperare;
		this.iDurataUtilizare = iDurataUtilizare;
		this.tipAmortizareAleasa = tipAmortizareAleasa;
	}

	public Integer getiElementActivNo() {
		return iElementActivNo;
	}

	public void setiElementActivNo(Integer iElementActivNo) {
		this.iElementActivNo = iElementActivNo;
	}

	public String getsDenumireElementActiv() {
		return sDenumireElementActiv;
	}

	public void setsDenumireElementActiv(String sDenumireElementActiv) {
		this.sDenumireElementActiv = sDenumireElementActiv;
	}

	public String getsAliasElementActiv() {
		return sAliasElementActiv;
	}

	public void setsAliasElementActiv(String sAliasElementActiv) {
		this.sAliasElementActiv = sAliasElementActiv;
	}

	public String getsDescriereElementActiv() {
		return sDescriereElementActiv;
	}

	public void setsDescriereElementActiv(String sDescriereElementActiv) {
		this.sDescriereElementActiv = sDescriereElementActiv;
	}

	public Date getDtDataIntrareGestiune() {
		return dtDataIntrareGestiune;
	}

	public void setDtDataIntrareGestiune(Date dtDataIntrareGestiune) {
		this.dtDataIntrareGestiune = dtDataIntrareGestiune;
	}

	public Date getDtDataIesireGestiune() {
		return dtDataIesireGestiune;
	}

	public void setDtDataIesireGestiune(Date dtDataIesireGestiune) {
		this.dtDataIesireGestiune = dtDataIesireGestiune;
	}

	public TipIncadrareGrupa geteIncadrareGrupa() {
		return eIncadrareGrupa;
	}

	public void seteIncadrareGrupa(TipIncadrareGrupa eIncadrareGrupa) {
		this.eIncadrareGrupa = eIncadrareGrupa;
	}

	public Double getdValoareIntrare() {
		return dValoareIntrare;
	}

	public void setdValoareIntrare(Double dValoareIntrare) {
		this.dValoareIntrare = dValoareIntrare;
	}

	public Double getdValoareRecuperare() {
		return dValoareRecuperare;
	}

	public void setdValoareRecuperare(Double dValoareRecuperare) {
		this.dValoareRecuperare = dValoareRecuperare;
	}

	public Integer getiDurataUtilizare() {
		return iDurataUtilizare;
	}

	public void setiDurataUtilizare(Integer iDurataUtilizare) {
		this.iDurataUtilizare = iDurataUtilizare;
	}

	public TipAmortizare getTipAmortizareAleasa() {
		return tipAmortizareAleasa;
	}

	public void setTipAmortizareAleasa(TipAmortizare tipAmortizareAleasa) {
		this.tipAmortizareAleasa = tipAmortizareAleasa;
	}
		
	
}
